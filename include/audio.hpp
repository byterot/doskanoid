#pragma once

class AudioManager {
  private:
    unsigned int duration;
  public:
    void bleep(unsigned int frequency, unsigned int duration);
    void update(void);
};
