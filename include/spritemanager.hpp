#pragma once
#include "sprite.hpp"
#include <vector>
#include <cstdint>

class SpriteManager {
  private:
    std::vector<Sprite*> sprites;

    void draw_sprite(Sprite* sprite);
    void undraw_sprite(Sprite* sprite);
  public:
    uint8_t bg_color;

    void add_sprite(Sprite* sprite);
    void remove_sprite(Sprite* sprite);
    
    void draw(void);
};

