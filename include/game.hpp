#pragma once
#include "spritemanager.hpp"
#include "audio.hpp"
#include "ball.hpp"
#include "block.hpp"
#include <vector>

class Game {
  private:
    unsigned char color_background = 18;
    unsigned char sprite_paddle[18*9+2] = { 18, 9,
      0, 0, 12,16,20,20,20,20,20,20,20,20,20,20,16,12,0, 0,
      0, 12,12,16,20,20,20,20,20,20,20,20,20,20,16,12,12,0,
      0, 40,30,16,30,30,30,30,30,30,30,30,30,30,16,30,40,0,
      11,40,40,16,24,24,24,24,24,24,24,24,24,24,16,40,40,11,
      11,40,40,16,26,26,26,26,26,26,26,26,26,26,16,40,40,11,
      11,40,40,16,26,26,26,26,26,26,26,26,26,26,16,40,40,11,
      0 ,4 ,4 ,16,24,24,24,24,24,24,24,24,24,24,16,4 ,4 ,0,
      0, 4 ,4 ,16,20,20,20,20,20,20,20,20,20,20,16,4 ,4 ,0,
      0 ,0, 4 ,16,20,20,20,20,20,20,20,20,20,20,16,4 ,0, 0
    };

    unsigned char sprite_ball[4*4+2] = { 4, 4,
      0, 15,15,0,
      15,11,11,15,
      15,11,11,15,
      0, 15,15,0
    };


    SpriteManager sprite_manager;
    AudioManager audio_manager;
    Sprite paddle;
    Ball ball;
    std::vector<Block*> blocks;
  
  public:
    Game();
    ~Game();
    
    void init_ball(void);
    void init(void);
    void quit(void);
    void update(void);
    void draw(void);
};
