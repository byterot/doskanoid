#pragma once
#include "pos.hpp"

class Sprite {
  friend class SpriteManager;
  private:
    unsigned char const* data;
    Pos last_position;
  public:
    unsigned char width, height;
    Pos position;

    Sprite();
    ~Sprite();
    void init(unsigned char const* data);
};
