#pragma once

struct Pos {
  float x, y;

  Pos(float x = 0, float y = 0)
    : x(x), y(y) { }

  Pos& operator=(Pos const& a) {
    x = a.x;
    y = a.y;
    return *this;
  }
};

inline Pos operator+(Pos const& a, Pos const& b) {
  return Pos(a.x + b.x, a.y + b.y);
};

inline Pos operator-(Pos const& a, Pos const& b) {
  return Pos(a.x - b.x, a.y - b.y);
};

inline Pos& operator+=(Pos& a, Pos const& b) {
  a = a + b;
  return a;
};

inline Pos& operator-=(Pos& a, Pos const& b) {
  a = a - b;
  return a;
};

inline bool operator==(Pos const& a, Pos const& b) {
  return (a.x == b.x && a.y == b.y);
};
inline bool operator!=(Pos const& a, Pos const& b) {
  return !(a == b);
}
