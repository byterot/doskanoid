#pragma once

class Block : public Sprite {
  public:
    static unsigned char constexpr sprite_block_blue[10*5+2] = { 10, 5,
      32,32,32,32,32,32,32,32,32,32,
      32,32,32,32,32,32,32,32,32,32,
      32,32,32,32,32,32,32,32,32,32,
      32,32,32,32,32,32,32,32,32,32,
      32,32,32,32,32,32,32,32,32,32
    };

    static unsigned char constexpr sprite_block_yellow[10*5+2] = { 10, 5,
      44,44,44,44,44,44,44,44,44,44,
      44,44,44,44,44,44,44,44,44,44,
      44,44,44,44,44,44,44,44,44,44,
      44,44,44,44,44,44,44,44,44,44,
      44,44,44,44,44,44,44,44,44,44
    };

    static unsigned char constexpr sprite_block_red[10*5+2] = { 10, 5,
      40,40,40,40,40,40,40,40,40,40,
      40,40,40,40,40,40,40,40,40,40,
      40,40,40,40,40,40,40,40,40,40,
      40,40,40,40,40,40,40,40,40,40,
      40,40,40,40,40,40,40,40,40,40
    };

    int health;
    Block(int h) {
      health = h;
      update();
    }
    void update(void) {
      switch(health) {
        case 1:
          init(sprite_block_blue);
          break;
        case 2:
          init(sprite_block_yellow);
          break;
        default:
          init(sprite_block_red);
          break;
      }
    }
};
