#include "game.hpp"
#include "base/vga.h"
#include "base/inp.h"

#include "sprite.hpp"
#include "block.hpp"

#include <stdlib.h>
#include <time.h>
#include <cstdio>
#include <math.h>

#define VGA_STRIDE 80
#define GAME_CEILING 50

Game::Game() {

}
Game::~Game() {
  
}

// preps the specified sprite for transperancy
void prep_sprite(unsigned char* sprite, unsigned char t) {
  unsigned char w,h;
  w = *sprite;
  h = *(sprite+1);
  sprite+=2;

  for(int i=0; i<w*h; ++i) {
    if(!*sprite) 
      *sprite = t;
  }
}

void Game::init(void) {
  srand(time(NULL));

  vga_setmode(0x13);
  prep_sprite(sprite_paddle, color_background);
  prep_sprite(sprite_ball, color_background);

  sprite_manager.bg_color = 0;

  paddle.init(sprite_paddle);
  paddle.position.x = 80 - paddle.width / 2;
  paddle.position.y = 195 - paddle.height;
  sprite_manager.add_sprite(&paddle);

  ball.init(sprite_ball);
  this->init_ball();
  sprite_manager.add_sprite(&ball);

  for(int j = 0; j < 5; ++j) {
    for(int i = 0; i < 13; ++i) {
      Block* b = new Block(1);
      b->position.x = 5 + i * 12;
      b->position.y = GAME_CEILING + 20 + j * 7;
      sprite_manager.add_sprite(b);
      blocks.push_back(b); 
    }
  }

  sprite_manager.draw(); // to update sprite positions

  unsigned int topbarcolor = 20 << 24 | 20 << 16 | 20 << 8 | 20;
  auto vga = vgaptr32();
  for(int i = 0; i < 80 * GAME_CEILING; ++i){
    *vga = topbarcolor;
    ++vga;
  }
}

void Game::init_ball(void) {
  ball.position.x = 80 - ball.width / 2;
  ball.position.y = 120;
  // throw ball in random direction
  // but reject horizontal angles
  int dir; 
  do {
    dir = rand() % 360;
  } while(dir > 300 || dir < 20 || dir > 160 && dir < 200);
  ball.dir_x = cos(dir) * 1.5;
  ball.dir_y = sin(dir) * 1.5;

}

void Game::quit(void) {
  vga_setmode(0x03);  
}

unsigned char ball_block_check(Ball& ball, Sprite* sprite) {
  unsigned char ret; // collides? right, up, left, down
  
  ret = ball.position.x + ball.width > sprite->position.x &&
    ball.position.x < sprite->position.x + sprite->width &&
    ball.position.y + ball.height > sprite->position.y &&
    ball.position.y < sprite->position.y + sprite->height;
  
  ret |= !(ball.position.x + ball.dir_x + ball.width > sprite->position.x &&
    ball.position.x + ball.dir_x < sprite->position.x + sprite->width &&
    ball.position.y + ball.height > sprite->position.y &&
    ball.position.y < sprite->position.y + sprite->height) << 1;
  
  ret |= !(ball.position.x + ball.width > sprite->position.x &&
    ball.position.x < sprite->position.x + sprite->width &&
    ball.position.y - ball.dir_y + ball.height > sprite->position.y &&
    ball.position.y - ball.dir_y < sprite->position.y + sprite->height) << 2;

  ret |= !(ball.position.x - ball.dir_x + ball.width > sprite->position.x &&
    ball.position.x - ball.dir_x < sprite->position.x + sprite->width &&
    ball.position.y + ball.height > sprite->position.y &&
    ball.position.y < sprite->position.y + sprite->height) << 3;

  ret |= !(ball.position.x + ball.width > sprite->position.x &&
    ball.position.x < sprite->position.x + sprite->width &&
    ball.position.y + ball.dir_y + ball.height > sprite->position.y &&
    ball.position.y + ball.dir_y < sprite->position.y + sprite->height) << 4;
  
  return ret;
}


void Game::update(void) {
  if(key_down(0x1E)) { // LEFT
    if(paddle.position.x > 0)
      paddle.position.x--;
  }
  if(key_down(0x20)) { // RIGHT
    if(paddle.position.x + paddle.width < 160)
      paddle.position.x++;
  }

  ball.position.x += ball.dir_x;
  ball.position.y += ball.dir_y;
  
  // Bounce
  if(ball.position.x < 0) {
    ball.position.x = 0;
    ball.dir_x = -ball.dir_x;
    audio_manager.bleep(300, 10);
  }
  if(ball.position.x + ball.width > 160) {
    ball.position.x = 160 - ball.width;
    ball.dir_x = -ball.dir_x;
    audio_manager.bleep(300, 10);
  }
  if(ball.position.y < GAME_CEILING) {
    ball.position.y = GAME_CEILING;
    ball.dir_y = -ball.dir_y;
    audio_manager.bleep(300, 10);
  }
  // Hit top of the paddle
  if(ball.position.y + ball.height >= paddle.position.y &&
      ball.position.x > paddle.position.x &&
      ball.position.x + ball.width < paddle.position.x + paddle.width) {
    ball.position.y = paddle.position.y - ball.height;
    ball.dir_y = -ball.dir_y;
    audio_manager.bleep(650, 10);
  }
  else {
    if(ball.position.y + ball.height >= paddle.position.y &&
        ball.position.x + ball.width > paddle.position.x &&
        ball.position.x < paddle.position.x + paddle.width) {
      ball.position.y = paddle.position.y - ball.height;
      ball.dir_y = -ball.dir_y;

      audio_manager.bleep(650, 10);
      // Left side of the paddle
      if(ball.position.x < paddle.position.x) {
        Pos lpos(paddle.position.x + 4, paddle.position.y + paddle.height / 2);
        Pos diff = ball.position - lpos;
        //ball.dir_x = cos(diff.x);
        //ball.dir_y = cos(diff.y);
        ball.dir_x = diff.x / 6;
        ball.dir_y = diff.y / 6;
      } else { // Right side of the paddle
        Pos lpos(paddle.position.x + paddle.width - 4, paddle.position.y + paddle.height / 2);
        Pos diff = ball.position - lpos;
        //ball.dir_x = cos(diff.x);
        //ball.dir_y = cos(diff.y);
        ball.dir_x = diff.x / 6;
        ball.dir_y = diff.y / 6;
      }
    }
  }

  // Reset
  if(ball.position.y + ball.height > 200) {
    init_ball();
    audio_manager.bleep(250, 25);
  }

  for(auto it = blocks.begin(); it != blocks.end(); ) { 
    auto sprite_ptr = *it;
    auto col = ball_block_check(ball, sprite_ptr); 

    if(col & 1) {
      sprite_ptr->health--;
      if(sprite_ptr->health == 0) {
      sprite_manager.remove_sprite(sprite_ptr);
      blocks.erase(it);
      } else ++it;
      audio_manager.bleep(450, 10);

      if(col & 0x2 || col & 0x8) ball.dir_x = -ball.dir_x;
      if(col & 0x4 || col & 0x10) ball.dir_y = -ball.dir_y;
    } else { 
      ++it;
    }
  }

  audio_manager.update();
}


void Game::draw(void) {
  sprite_manager.draw();

  vga_sync();
}
