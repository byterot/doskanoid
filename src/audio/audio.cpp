#include "audio.hpp"
#include "base/snd.h"

void AudioManager::bleep(unsigned int frequency, unsigned int duration) {
  this->duration = duration;
  beep(frequency);
};

void AudioManager::update(void) {
  if(duration > 0){
    duration--;
    if(duration == 0)
      nobeep();
  }
};
