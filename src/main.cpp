#include <sys/nearptr.h>
#include <iostream>

#include "base/vga.h"
#include "base/inp.h"

#include "game.hpp"

#include "pos.hpp"

int main()
{   
  if(!__djgpp_nearptr_enable())
    return 1;

  keyboard_bind();

  Game g;
  g.init();

  while(1)
  {
    keyboard_fetch();
    if(key_pressed(0x1)) // ESCAPE
      break;

    g.update();
    g.draw();
  }

  g.quit();
  keyboard_unbind();
  __djgpp_nearptr_disable();
  return 0;
}
