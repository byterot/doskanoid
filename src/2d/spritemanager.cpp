#include "spritemanager.hpp"
#include "sprite.hpp"
#include <vector>
#include <cstdint>
#include "base/vga.h"
#include <stdio.h>

void SpriteManager::add_sprite(Sprite* sprite) {
  sprites.push_back(sprite);
}

void SpriteManager::remove_sprite(Sprite* sprite) {
  for(auto it = sprites.begin(); it != sprites.end(); ) { 
    if(*it == sprite) {
      this->undraw_sprite(sprite);
      sprites.erase(it);
    }
    else ++it;
  }
}

void SpriteManager::draw_sprite(Sprite* sprite) {
  uint16_t const stride = 160;
  uint16_t volatile* vga = vgaptr16();

  uint8_t const* td = sprite->data + 2;

  vga += (int)sprite->position.y * stride + (int)sprite->position.x;

  for(int y = 0; y < sprite->height; ++y) {
    for(int x = 0; x < sprite->width; ++x) {
      *vga = *td << 8 | *td;
      td++;
      vga++;
    }
    vga += stride - sprite->width;
  }
}

void SpriteManager::undraw_sprite(Sprite* sprite) {
  uint16_t const stride = 160;
  uint16_t volatile* vga = vgaptr16();
  
  vga += (int)sprite->last_position.y * stride + (int)sprite->last_position.x;

  for(int y = 0; y < sprite->height; ++y) {
    for(int x = 0; x < sprite->width; ++x) {
      *vga = bg_color << 8 | bg_color;
      vga++;
    }
    vga += stride - sprite->width;
  }
}

void SpriteManager::draw(void) {
  for(auto it : sprites) {
    if(it->last_position != it->position) {
      undraw_sprite(it);
      draw_sprite(it);
      it->last_position = it->position;
    }
  }
}
