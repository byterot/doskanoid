#!/bin/bash
set -e
/usr/local/djgpp/bin/i586-pc-msdosdjgpp-g++ -std=gnu++17 -s -o out.exe -I include/ -I src/ src/*.cpp src/game/*.cpp src/2d/*.cpp src/base/*.c src/audio/*.cpp
dosbox -conf dosbox.conf -c "mount x $PWD" -c "x:" -c "out.exe" #-c "exit"
echo "Application output:"
#cat OUT.TXT

